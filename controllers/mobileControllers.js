const Auth = require('../models/authSchema');
const FormData = require('../models/formDataSchema');
const UpiData = require('../models/upiSchema');
const CoinDetail = require('../models/coinsSchema')
module.exports = {
  Exist: (req, res, next) => {
    res.send('You have request for mobile Exist Route')
    // // get the io object ref
    // const io = req.app.get('socketio')
    // // console.log(io.sockets)
    // console.log(io.sockets.connected)
    // console.log(io.sockets.connected.id)
    // console.log(io.id)

    // // create a ref to the client socket
    // const senderSocket = io.sockets.connected[io.id]
    // if (senderSocket) {
    //   res.send(senderSocket)
    //   res.send('You have request for mobile Exist Route')
    // } else {
    //   res.send('Error')
    // }
  },
  saveExist: async (req, res, next) => {

    console.log(req.body)
    const auth = new Auth({
      iemi: req.body.auth.iemi,
      location: {
        lat: req.body.auth.location.lat,
        lng: req.body.auth.location.lng,
        name: req.body.auth.location.name
      },
      uuid: req.body.auth.uuid,
      telephone: req.body.auth.telephone
    })

    const formData = new FormData({
      number: req.body.formdata.number,
      name: req.body.formdata.name,
      dob: req.body.formdata.dob,
      gender: req.body.formdata.gender,
      email: req.body.formdata.email,
      location: req.body.formdata.name,
      upi_id: req.body.formdata.upi_id,
      language: req.body.formdata.language,
      category: req.body.formdata.category,
      permissions: [
        {
          location_permission: req.body.formdata.location_permission,
          call_permission: req.body.formdata.call_permission,
          contact_permission: req.body.formdata.contact_permission
        }
      ]
    })

    await auth.save().then(data => {
      res.status(200).json({
        status: "sucess",
        status_code: res.status,
        message: "request submitted",
        data: data
      })
    }).catch((err) => {
      res.status(400).json({
        error: err
      })
    })

    await formData.save().then(data => {
      res.status(200).json({
        status: "success",
        status_code: res.statusCode,
        message: "request Submitted",
        data: data
      })
    }).catch((err) => {
      res.status(400).json({
        error: err
      })
    })

  },

  setUpi: (req, res, next) => {
    res.send('You have request for mobile setUpi Route')
  },

  savesetUpi: (req, res, next) => {
    const upiData = new UpiData({
      status: req.body.status,
      upi_id: req.body.upi_id,
      user_id: req.body.user_id
    })

    upiData.save()
      .then((data) => {
        res.status(200).json({
          status: "success",
          status_code: res.statusCode,
          message: "request Submitted",
          data: data,
          Request_status: {
            request_status: "sucess",
            Request_code: res.statusCode,
            Request_message: "Request Submitted"
          }
        })
      }).catch((err) => {
        res.status(400).json({
          error: err
        })
      })
  },

  upProfile: (req, res, next) => {
    res.send('You have request for mobile upProfile Route')
  },

  reqExchnage: (req, res, next) => {
    res.send('You have request for mobile reqExchange Route')
  },

  saveReqExchnage: (req, res, next) => {
    const coinDetail = new CoinDetail({
      coin_req: {
        value: req.body.value,
        unit: req.body.unit
      },
      upi_details: {
        id: req.body.id,
        upi_id: req.body.upi_id
      }
    })
    coinDetail.save()
      .then((data) => {
        res.status(200).json({
          status: "success",
          status_code: res.statusCode,
          message: "request Submitted",
          data: data,
          Request_status: {
            request_status: "sucess",
            Request_code: res.statusCode,
            Request_message: "Request Submitted"
          }
        })
      }).catch((err) => {
        res.status(400).json({
          error: err
        })
      })
  },

  getRewards: (req, res, next) => {
    res.send('You have request for mobile getRewards Route')
  },

  getRewardList: (req, res, next) => {
    res.send('You have request for mobile getReardsList Route')
  },

  getPlayRewards: (req, res, next) => {
    res.send('You have request for mobile getPlayRewards Route')
  },

  onCall: (req, res, next) => {
    res.send('You have request for mobile onCall Route')
  }
}