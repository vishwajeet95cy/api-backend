const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const session = require('express-session');
const cookie = require('cookie-parser');
const logger = require('./config/logger')
const mongostore = require('connect-mongo')(session)
const server = require('http').createServer(app);
const io = require('socket.io')(server)
const cors = require('cors');
const mongoose = require('mongoose');
const morgan = require('morgan')
const dotenv = require('dotenv');
const webRoutes = require('./routes/webRoutes');
const mobileRoutes = require('./routes/mobileRoutes')
dotenv.config();
const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');
const passport = require('passport');
require('./config/passport');

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Sample API',
      description: 'Sample API',
      servers: ["http://localhost:5000"]
    }
  },
  apis: ["./routes/mobileRoutes.js"]
};

const swaggerDocs = swaggerJsdoc(swaggerOptions);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

mongoose.connect(process.env.DB_HOST, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }, (err, db) => {
  if (err) throw err;
  console.log('Database Connected')
});

app.use(cookie());
app.use(session({
  secret: process.env.Secret,
  resave: false,
  saveUninitialized: true,
  store: new mongostore({ url: process.env.DB_HOST }),
  cookie: { maxAge: 60 * 24 }
}))
app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  console.log(req.session)
  next()
})


app.set('socketio', io);
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/public');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.get('/', (req, res) => {
  res.render('index.html')
})

io.on('connection', (socket) => {
  console.log('Socket Connected');
  console.log("socket: " + socket.id)

  socket.emit('data', 'Hello World')

  socket.on('disconnect', () => {
    console.log('Socket Disconnect')
  })
})


app.use('/mobile', mobileRoutes);
app.use('/web', webRoutes)
server.listen(process.env.PORT, () => {
  console.log(`Server is listen at: http://localhost:${process.env.PORT}`);
})
