const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const UserDetails = require('../models/userSchema')


passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});

passport.use('google', new GoogleStrategy({
  clientID: process.env.GOOGLE_ID,
  clientSecret: process.env.GOOGLE_SECRET,
  callbackURL: "http://localhost:5000/auth/google/callback"
},
  (request, accessToken, refreshToken, params, profile, done) => {
    UserDetails.findOne({ google: profile.id }).then((currentUser, profile) => {
      if (currentUser) {
        console.log('user is:' + currentUser);
        done(null, currentUser)
      } else {
        new UserDetails({
          profile: {
            location: profile._json.locale,
            name: profile.displayName,
            website: '',
            photos: profile._json.picture
          },
          tokens: [{
            kind: profile.provider,
            TokenType: params.token_type,
            AccessToken: accessToken,
            RefreshToken: refreshToken,
            accessTokenExpires: new Date().toLocaleString(params.expires_in)
          }],
          emailVerficationToken: "",
          email: profile.emails[0].value,
          password: "",
          google: profile.id
        }).save().then((data) => {
          console.log('New User created' + data);
          done(null, data)
        }).catch((err) => {
          console.log(err)
        })
      }
    }).catch((err) => {
      console.log(err)
    });
  }
));

passport.use('facebook', new FacebookStrategy({
  clientID: process.env.FACEBOOK_ID,
  clientSecret: process.env.FACEBOOK_SECRET,
  callbackURL: "http://localhost:8000/auth/facebook/callback",
  passReqToCallback: true,
  profileFields: ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone',
    'updated_time', 'verified', 'displayName']
},
  (request, accessToken, refreshToken, params, profile, done) => {
    UserDetails.findOne({ facebook: profile.id }).then((currentUser) => {
      if (currentUser) {
        console.log('user is:' + currentUser);
        done(null, currentUser)
      } else {
        new UserDetails({
          profile: {
            location: '',
            name: profile.displayName,
            website: '',
            photos: ''
          },
          tokens: [{
            kind: profile.provider,
            TokenType: params.token_type,
            AccessToken: accessToken,
            RefreshToken: refreshToken,
            accessTokenExpires: new Date().toLocaleString(params.expires_in)
          }],
          emailVerficationToken: "",
          email: profile.emails[0].value,
          password: "",
          facebook: profile.id
        }).save().then((data) => {
          console.log('New User created' + data);
          done(null, data)
        }).catch((err) => {
          console.log(err)
        })
      }
    }).catch((err) => {
      console.log(err)
    });
  }
));

module.exports.passport = passport