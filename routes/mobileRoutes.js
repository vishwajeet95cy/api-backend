const router = require('express').Router();
const { Exist, setUpi, upProfile, reqExchnage, getRewardList, getPlayRewards, onCall, getRewards, saveExist, savesetUpi, saveReqExchnage } = require('../controllers/mobileControllers');

/**
 * @swagger
 * /mobile/exist:
 *   get:
 *     description: Use to see the exist route is working
 *     produces:
 *       - application/json
 *     responses:
 *        200:
 *         description: A successful response
 */

/**
 * @swagger
 * /mobile/setupi:
 *   get:
 *     description: Use to see the setUpi route is working
 *     produces:
 *       - application/json
 *     responses:
 *        '200':
 *         description: A successful response
 */

/**
* @swagger
* /mobile/up_profile:
*   get:
*     description: Use to see the upProfile route is working
*     produces:
*       - application/json
*     responses:
*        '200':
*         description:A successful response
*/

/**
* @swagger
* /mobile/req_exchange:
*   get:
*     description: Use to see the req exchange route is working
*     produces:
*       - application/json
*     responses:
*        '200':
*         description:A successful response
*/

/**
* @swagger
* /mobile/get_rewards_a:
*   get:
*     description: Use to see the get rewards route is working
*     produces:
*       - application/json
*     responses:
*        200:
*         description:A successful response
*/

/**
* @swagger
* /mobile/get_rewards_list:
*   get:
*     description: Use to see the get rewards list route is working
*     produces:
*       - application/json
*     responses:
*        200:
*         description:A successful response
*/

/**
* @swagger
* /mobile/get_play_rewards:
*   get:
*     description: Use to see the get play rewards route is working
*     produces:
*       - application/json
*     responses:
*        200:
*         description:A successful response
*/

/**
* @swagger
* /mobile/onCall:
*   get:
*     description: Use to see the on Call route is working
*     produces:
*       - application/json
*     responses:
*        200:
*         description:A successful response
*/

/**
 * @swagger
 * /mobile/exist:
 *   post:
 *     summary: Create Auth data and Form data
 *     produces:
 *        - application/json
 *        - x-www-form-urlencoded
 *     parameters:
 *        - in: body
 *          name: user
 *          description: The user auth data and form data
 *          schema:
 *            type: object
 *            required:
 *               - iemi
 *               - lat
 *               - lng
 *               - uuid
 *               - telephone
 *               - number
 *               - name
 *               - dob
 *               - gender
 *               - email
 *               - location
 *               - upi_id
 *               - language
 *               - category
 *               - location_permission
 *               - call_permission
 *               - contact_permission
 *            properties:
  *               iemi:
  *                 type: string
  *               lat:
  *                 type: string
  *               lng:
  *                 type: string
  *               name:
  *                 type: string
  *               uuid:
  *                 type: string
  *               telephone:
  *                 type: string
  *               number:
  *                 type: string
  *               dob:
  *                 type: string
  *               gender:
  *                 type: string
  *               email:
  *                 type: string
  *               location:
  *                 type: string
  *               upi_id:
  *                 type: string
  *               language:
  *                 type: array
  *               category:
  *                 type: array
  *               location_permission:
  *                 type: boolean
  *               call_permission:
  *                 type: boolean
  *               contact_permission:
  *                 type: boolean
 *     responses:
 *         200:
 *          description: A user auth data and form data
 */

/**
 * @swagger
 * /mobile/setupi:
 *   post:
 *     summary: Create setUpi data
 *     produces:
 *        - application/json
 *        - x-www-form-urlencoded
 *     parameters:
 *        - in: body
 *          name: user
 *          description: The user setUpi data
 *          schema:
 *            type: object
 *            required:
 *               - status
 *               - upi_id
 *               - user_id
 *            properties:
 *                status:
 *                  type: boolean
 *                upi_id:
 *                  type: string
 *                user_id:
 *                  type: string
 *     responses:
 *         200:
 *          description: A user upiData
 */

/**
* @swagger
* /mobile/req_exchange:
*   post:
*     summary: Create user coin details
*     produces:
*        - application/json
*        - x-www-form-urlencoded
*     parameters:
*        - in: body
*          name: user
*          description: The user coin detail
*          schema:
*            type: object
*            required:
*               - value
*               - unit
*               - id
*               - upi_id
*            properties:
 *               value:
 *                 type: string
 *               unit:
 *                 type: string
 *               id:
 *                 type: string
 *               upi_id:
 *                 type: string
*     responses:
*         200:
*          description: A user cain details
*/


router.get('/exist', Exist)

router.post('/exist', saveExist)

router.get('/setupi', setUpi)

router.post('/setupi', savesetUpi)

router.get('/up_profile', upProfile);

router.get('/req_exchange', reqExchnage);

router.post('/req_exchange', saveReqExchnage);

router.get('/get_rewards_a', getRewards);

router.get('/get_rewards_list', getRewardList);

router.get('/get_play_rewards', getPlayRewards);

router.get('/onCall', onCall);



module.exports = router