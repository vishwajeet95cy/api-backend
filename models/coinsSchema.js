const mongoose = require('mongoose');

const coinSchema = new mongoose.Schema({
  coin_req: {
    value: {
      type: String
    },
    unit: {
      type: String
    }
  },
  upi_details: {
    id: {
      type: String
    },
    upi_id: {
      type: String
    }
  }
}, {
  timestamps: Date
})

module.exports = mongoose.model('CoinDetail', coinSchema)