const mongoose = require('mongoose');

const upiSchema = new mongoose.Schema({
  status: {
    type: Boolean
  },
  upi_id: {
    type: String
  },
  user_id: {
    type: String
  }
}, {
  timestamps: Date
})

module.exports = mongoose.model('UpiData', upiSchema)