const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  tokens: {
    type: Array
  },
  email: {
    type: String
  },
  password: {
    type: String
  },
  emailVerficationToken: {
    type: String
  },
  profile: {
    type: Object
  },
  google: {
    type: String
  },
  facebook: {
    type: String
  }
}, {
  timestamps: Date
})


module.exports = mongoose.model('UserDetails', UserSchema)