const mongoose = require('mongoose');

const authSchema = new mongoose.Schema({
  iemi: {
    type: String
  },
  location: {
    lat: {
      type: String,
    },
    lng: {
      type: String
    },
    name: {
      type: String
    }
  },
  uuid: {
    type: String
  },
  telephone: {
    type: String
  }
}, {
  timestamps: Date
})

module.exports = mongoose.model('Auth', authSchema);