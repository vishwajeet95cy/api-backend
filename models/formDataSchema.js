const mongoose = require('mongoose');

const formSchema = new mongoose.Schema({
  number: {
    type: String
  },
  name: {
    type: String
  },
  dob: {
    type: String
  },
  gender: {
    type: String
  },
  email: {
    type: String,
    unique: true
  },
  location: {
    type: String
  },
  date: {
    type: Date,
    default: new Date
  },
  upi_id: {
    type: String
  },
  language: {
    type: Array
  },
  category: {
    type: Array
  },
  permissions: [
    {
      location_permission: {
        type: Boolean
      },
      call_permission: {
        type: Boolean
      },
      contact_permission: {
        type: Boolean
      }
    }
  ]
}, {
  timestamps: Date
})

module.exports = mongoose.model('FormData', formSchema)